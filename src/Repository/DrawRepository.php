<?php

namespace App\Repository;

use App\Entity\Draw;

interface DrawRepository
{
    public function find(int $id): Draw;

    public function findClosestToTarget(): \Iterator;

    public function save(Draw $draw): void;
}