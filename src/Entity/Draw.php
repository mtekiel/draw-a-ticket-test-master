<?php

namespace App\Entity;

class Draw
{
    // ID
    // Title
    // Ticket target
    // Tickets
    // Date created
    // Date last ticket was entered

    private ?int $id = null;

    public function __construct(
        private string $title
    ){
        // ...
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
